# Playing with Jupyter Widgets

[[_TOC_]]

## Introduction

### test_plot.ipynb

This is a Jupyter Notebook, which means that it can be run in VS Code or Binder.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/open7373636%2Fjupyter-widgets/HEAD?labpath=test_plot.ipynb)

This notebook makes use of [ipywidgets](https://ipywidgets.readthedocs.io/en/stable/) and especially the [interact](https://ipywidgets.readthedocs.io/en/stable/examples/Using%20Interact.html) function.

### test_embed.py

This code is taken from https://ipywidgets.readthedocs.io/en/latest/embedding.html#python-interface

I copied the example given into `test_embed.py`.

## System requirements

Only tested on Windows 10, but should work on any Operating System that supports Python.

## Installation

Install Python 3.9, using the Microsoft Store

Open a terminal (Command Prompt or PowerShell) in the folder you are using to hold this code. (We'll refer to it as your Python folder from now on)

Create a virtual environment:
```
> python -m venv env3.9
```

Activate it:
```
> .\env3.9\Scripts\activate
```

Upgrade pip:
```
> python -m pip install --upgrade pip
```

Install ipywidgets:
```
> python -m pip install jupyter
```

Install numpy:
```
> python -m pip install numpy
```

Install matplotlib:
```
> python -m pip install matplotlib
```

## Running

* Open a terminal (Command Prompt, PowerShell, or a built-in terminal in your favourite editor) in the `jupyter-widgets` folder.

* Activate the virtual environment:
```
> .\env3.9\Scripts\activate
```

* If you are using VS Code, just install the Jupyter extension. You can then run the notebook directly in VS Code.

* Alternatively, you can open the notebook in [Binder](https://mybinder.org/), which is a free JupyterHub server hosted in the cloud. However, your Git repo must be public in order for Binder to be able to read from it!

## To-Do

* How to create a HTML file that has both Jupyter widgets and a graphing library?

e.g. Plotly: https://plotly.com/python/interactive-html-export/

Or add Dash on top of Plotly: https://community.plotly.com/t/how-to-deploy-dash-app-internally-in-local-network/61312

If I need a public-facing web server: https://chrisvoncsefalvay.com/2019/08/28/deploying-dash-on-amazon-ecs/

* Try JulyterLite

See https://ipywidgets.readthedocs.io/en/latest/_static/lab/index.html